#!/usr/bin/env bash

VIM_SETTINGS=`pwd`
ln -s $VIM_SETTINGS/vimrc $HOME/.vimrc
ln -s $VIM_SETTINGS $HOME/.vim
